<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'keranjang';
	protected $primaryKey = 'id';
    protected $fillable = [
        'qty',
        'user_id',
        'produk_id',    
    ];
    public function product(){
        return $this->belongsTo('App\Produk','produk_id');
    }
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
