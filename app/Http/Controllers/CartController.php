<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Kurir;
use App\Payment;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index(){
        $kurir=Kurir::get();
        $payment=Payment::get();
        $cart= Cart::get();
        if (session(key:'success_message')) {
            Alert::success('Berhasil!', session(key:'success_message'));
        }
        return view('backend.pages.user.cart', compact('cart','payment','kurir'));
    }
    public function store(Request $request,$id)
    {
        $request->validate(
            [
                'qty'=>'required',
            ],
            [
                'qty.required'=>'Inputan jumlah produk harus diisi',
            ]
        );
        $cart =Cart::where('user_id',Auth::user()->id)
                ->where('produk_id',$id)
                ->first();
        
        if($cart){
            $cart['qty']= $cart['qty']+$request->qty;
            $cart->update();
        }else{
            Cart::create([
                'user_id' => Auth::user()->id,
                'produk_id' => $id,
                'qty'=>$request->qty,
            ]);
        }

        return redirect('/cart')->withSuccessMessage("Berhasil Menambahkan Cart");
    }
    public function destroy($id)
    {
       Cart::where('id',$id)->delete();
       return redirect('/cart')->withSuccessMessage("Berhasil Menghapus Cart");
    }
}
