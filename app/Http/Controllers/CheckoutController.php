<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Order;
use App\OrderDetail;
use App\Kurir;
use PDF;

class CheckoutController extends Controller
{
    public function proses(Request $request){
        $code="STORE-". mt_rand(00000,99999);
        $carts=Cart::with(['product','user'])
            ->where('user_id',Auth::user()->id)
            ->get();
        $order= Order::create([
            'user_id'=>Auth::user()->id,
            'status'=>'pending',
            'payment_id'=>$request->payment,
            'kurir_id'=>$request->kurir,
            'total'=>$request->total,
            'kode'=>$code,
        ]);
        foreach ($carts as $cart) {
            $trx="TRX-". mt_rand(00000,99999);  
            OrderDetail::create([
                'order_id'=>$order->id,
                'status'=>'pending',
                'harga'=>$cart->product->harga,
                'qty'=>$cart->qty,
                'produk_id'=>$cart->product->id,
                'kode'=>$trx,
            ]);
        }
        Cart::where('user_id', Auth::user()->id)->delete();
        return redirect('/checkout')->withSuccessMessage("Berhasil Menambahkan Pesanan");
    }
    public function index()
    {
        $order=Order::where('user_id',Auth::user()->id)->get();
        if (session(key:'success_message')) {
            Alert::success('Berhasil!', session(key:'success_message'));

        }
        return view('backend.pages.user.checkout', compact('order'));
    }
    public function generatePDF($id){
        $order=Order::where('id',$id)->first();

        $pdf = PDF::loadView('backend.pages.user.invoice', compact('order'));
        return $pdf->download('laporan-pdf.pdf');
    }
}
