<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Produk;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $roles=Auth::user()->role;
        switch($roles){
            case '1' ://admin
                return $this->dasboardAdmin();
                break;
            default:
                return $this->indexUser();
        }
    }
    protected function dasboardAdmin(){
        $produk= Produk::get();
        return view('backend.pages.admin.produk.index',compact("produk"));
      }
    protected function indexUser(){
        $produk= Produk::get();
        return view('backend.pages.user.index',compact('produk'));
    }
}
