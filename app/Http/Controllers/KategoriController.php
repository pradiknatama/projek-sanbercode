<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use RealRashid\SweetAlert\Facades\Alert;

class KategoriController extends Controller
{
    public function index()
    {
        $kategori= Kategori::get();


        if (session(key:'success_message')) {
            Alert::success('Berhasil!', session(key:'success_message'));

        }

        return view('backend.pages.admin.kategori.index', compact('kategori'));
    }
    public function create(){
        return view('backend.pages.admin.kategori.create');
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama'=>'required',
            ],
            [
                'nama.required'=>'Inputan nama kategori harus diisi',
            ]
        );
        Kategori::insert(
            [
                'nama'=>$request['nama'],
            ]
        );
        return redirect('/kategori')->withSuccessMessage("Berhasil Menambahkan Kategori");
    }
    public function edit($id)
    {
        $kategori= Kategori::where('id',$id)->first();
        return view('backend.pages.admin.kategori.show',compact('kategori'));
    }
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama'=>'required',
            ],
            [
                'nama.required'=>'Inputan nama kategori harus diisi',
            ]
        );
        Kategori::where('id',$id)
            ->update(
                [
                    'nama'=>$request['nama'],
                ]);
        return redirect('/kategori')->withSuccessMessage("Berhasil Mengubah Kategori");
    }
    public function destroy($id)
    {
       Kategori::where('id',$id)->delete();
       return redirect('/kategori')->withSuccessMessage("Berhasil Menghapus Kategori");
    }
}
