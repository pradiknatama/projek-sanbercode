<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kurir;
use RealRashid\SweetAlert\Facades\Alert;

class KurirController extends Controller
{
    public function index()
    {
        $kurir= Kurir::get();


        if (session(key:'success_message')) {
            Alert::success('Berhasil!', session(key:'success_message'));

        }

        return view('backend.pages.admin.kurir.index', compact('kurir'));
    }
    public function create(){
        return view('backend.pages.admin.kurir.create');
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama'=>'required',
                'harga'=>'required',
            ],
            [
                'nama.required'=>'Inputan nama kurir harus diisi',
                'rekening.required'=>'Inputan harga harus diisi',
            ]
        );
        Kurir::insert(
            [
                'nama'=>$request['nama'],
                'harga'=>$request['harga'],
            ]
        );
        return redirect('/kurir')->withSuccessMessage("Berhasil Menambahkan Kurir");
    }
    public function edit($id)
    {
        $kurir= Kurir::where('id',$id)->first();
        return view('backend.pages.admin.kurir.show',compact('kurir'));
    }
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama'=>'required',
                'harga'=>'required',
            ],
            [
                'nama.required'=>'Inputan nama kurir harus diisi',
                'rekening.required'=>'Inputan harga harus diisi',
            ]
        );
        Kurir::where('id',$id)
            ->update(
                [
                    'nama'=>$request['nama'],
                    'harga'=>$request['harga'],
                ]);
        return redirect('/kurir')->withSuccessMessage("Berhasil Mengubah Kurir");
    }
    public function destroy($id)
    {
        Kurir::where('id',$id)->delete();
       return redirect('/kurir')->withSuccessMessage("Berhasil Menghapus Kurir");
    }
}
