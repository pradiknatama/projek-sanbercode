<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use RealRashid\SweetAlert\Facades\Alert;

class PaymentController extends Controller
{
    public function index()
    {
        $payment= Payment::get();


        if (session(key:'success_message')) {
            Alert::success('Berhasil!', session(key:'success_message'));

        }

        return view('backend.pages.admin.payment.index', compact('payment'));
    }
    public function create(){
        return view('backend.pages.admin.payment.create');
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama'=>'required',
                'rekening'=>'required',
            ],
            [
                'nama.required'=>'Inputan nama payment harus diisi',
                'rekening.required'=>'Inputan No rekening harus diisi',
            ]
        );
        Payment::insert(
            [
                'nama'=>$request['nama'],
                'rekening'=>$request['rekening'],
            ]
        );
        return redirect('/payment')->withSuccessMessage("Berhasil Menambahkan Payment");
    }
    public function edit($id)
    {
        $payment= Payment::where('id',$id)->first();
        return view('backend.pages.admin.payment.show',compact('payment'));
    }
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama'=>'required',
                'rekening'=>'required',
            ],
            [
                'nama.required'=>'Inputan nama payment harus diisi',
                'rekening.required'=>'Inputan No rekening harus diisi',
            ]
        );
        Payment::where('id',$id)
            ->update(
                [
                    'nama'=>$request['nama'],
                    'rekening'=>$request['rekening'],
                ]);
        return redirect('/payment')->withSuccessMessage("Berhasil Mengubah Payment");
    }
    public function destroy($id)
    {
        Payment::where('id',$id)->delete();
       return redirect('/payment')->withSuccessMessage("Berhasil Menghapus Payment");
    }
}
