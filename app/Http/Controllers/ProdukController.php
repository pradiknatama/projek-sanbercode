<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;


class ProdukController extends Controller
{
    //admin
    public function index()
    {
        $produk= Produk::get();


        if (session(key:'success_message')) {
            Alert::success('Berhasil!', session(key:'success_message'));

        }

        return view('backend.pages.admin.produk.index', compact('produk'));
    }
    public function create(){
        $kategori= Kategori::get();
        return view('backend.pages.admin.produk.create',compact('kategori'));
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama'=>'required',
                'harga'=>'required',
                'stock'=>'required',
                'deskripsi'=>'required',
                'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

            ],
            [
                'nama.required'=>'Inputan nama produk harus diisi',
                'harga.required'=>'Inputan harga produk harus diisi',
                'stock.required'=>'Inputan stock produk harus diisi',
                'deskripsi.required'=>'Inputan deskripsi produk harus diisi',
                'foto.required'=>'Inputan foto produk harus diisi',
            ]
        );
        $filename = time().'.'.$request->foto->extension();  
        $request->foto->move(public_path('images/produk'), $filename);
        $produk=new Produk;
        $produk->nama=$request->nama;
        $produk->harga=$request->harga;
        $produk->stock=$request->stock;
        $produk->deskripsi=$request->deskripsi;
        $produk->foto=$filename;
        $produk->kategori_id=$request->kategori;
        $produk->save();
        return redirect('/produk')->withSuccessMessage("Berhasil Menambahkan Produk");
    }
    public function edit($id)
    {
        $produk= Produk::where('id',$id)->first();
        return view('backend.pages.admin.produk.show',compact('produk'));
    }
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama'=>'required',
                'harga'=>'required',
                'stock'=>'required',
                'deskripsi'=>'required',
            ],
            [
                'nama.required'=>'Inputan nama produk harus diisi',
                'harga.required'=>'Inputan harga produk harus diisi',
                'stock.required'=>'Inputan stock produk harus diisi',
                'deskripsi.required'=>'Inputan deskripsi produk harus diisi',
            ]
        );
        $edit=Produk::find($id);
        if ($request->foto=="") {
            $filename=$edit->foto;
        }else{
            $filename = time().'.'.$request->foto->extension();  
            $request->foto->move(public_path('images/produk'), $filename);
        }
        
            $edit->update(
                [
                    'nama'=>$request['nama'],
                    'harga'=>$request['harga'],
                    'stock'=>$request['stock'],
                    'deskripsi'=>$request['deskripsi'],
                    'foto'=>$filename,
                    'kategori_id'=>$request['kategori'],
                ]);
        return redirect('/produk')->withSuccessMessage("Berhasil Mengubah Produk");
    }
    public function destroy($id)
    {
       Produk::where('id',$id)->delete();
       return redirect('/produk')->withSuccessMessage("Berhasil Menghapus Produk");
    }
    // user
    public function user_show(){
        $produk=Produk::get();
        return view('backend.pages.user.index',compact('produk'));
    }
    public function user_detail($id){
        $produk= Produk::where('id',$id)->first();
        return view('backend.pages.user.detail',compact('produk'));
    }
}
