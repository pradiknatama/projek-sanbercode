<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurir extends Model
{
    protected $table = 'kurir';
	protected $primaryKey = 'id';
    protected $fillable = [
        'nama','harga'
    ];
    public function order(){
        return $this->hasOne('App\Order');
    }
}
