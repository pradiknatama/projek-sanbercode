<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
	protected $primaryKey = 'id';
    protected $fillable = [
        'total',
        'status',
        'user_id',
        'payment_id',
        'kurir_id',
        'total',
        'kode',
    ];
    public function payment(){
        return $this->belongsTo('App\Payment','payment_id');
    }
    public function details(){
        return $this->hasMany('App\OrderDetail');
    }
    public function kurir(){
        return $this->belongsTo('App\Kurir','kurir_id');
    }
    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
}
