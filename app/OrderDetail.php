<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_detail';
	protected $primaryKey = 'id';
    protected $fillable = [
        'harga',
        'qty',
        'kode',
        'produk_id',
        'order_id',
        'status'
    ];
    public function orders(){
        return $this->belongsTo('App\Order','order_id');
    }
    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
}
