<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payment';
	protected $primaryKey = 'id';
    protected $fillable = [
        'nama','rekening'
    ];
    public function order(){
        return $this->hasOne('App\Order');
    }
}
