<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
	protected $primaryKey = 'id';
    protected $fillable = [
        'nama',
        'harga',
        'stock',
        'deskripsi',
        'foto',
        'kategori_id'
    ];
    public function category(){
        return $this->belongsTo('App\Kategori','kategori_id');
    }
    public function keranjang(){
        return $this->hasMany('App\Cart');
    }
}
