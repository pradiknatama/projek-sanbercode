<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profil';
	protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'phone',
        'alamat',
    ];
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
