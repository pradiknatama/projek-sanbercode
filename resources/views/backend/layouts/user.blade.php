<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>
            @yield('judul') 3A Store
        </title>
        <link rel = "shortcut icon" type = "image/png" href = "{{ asset('images\logoKecil.png') }}">
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
        <!-- Core theme CSS (includes Bootstrap)-->
        @stack("style")
        <link href="{{ asset('template1/css/styles.css') }}" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        @guest
            @include('backend.includes.nav')
        @else
            @if(Auth::User()->role==1)
                @include('backend.includes.nav_admin')
            @else
                @include('backend.includes.nav-auth')
            @endif
        @endguest
        <!-- Section-->
        <section class="py">
            <div class="">
                @yield('header')
            </div>
            <div class="container px-4 px-lg-5 mt-5">
                @yield('content')
            </div>
        </section>
        <div class="footer">
            <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> 
            All rights reserved.
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('/admin/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Core theme JS-->
        @include('sweetalert::alert')
        @stack('scripts')
        <script src="{{ asset('template1/js/scripts.js') }}"></script>
        
    </body>
</html>
