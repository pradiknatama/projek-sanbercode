@extends("backend.layouts.user")
@push("style")
<link rel="stylesheet" href="/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="/admin/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush

@section('judul')
    Dashboard |
@endsection

@section("content")
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Kategori</h3>
    </div>
    <div class="card-body p-0">
        <a href="/kategori/create" class="btn btn-primary m-3">Tambah Kategori</a>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>#</th>
              <th>Nama</th>
              <th>Action</th>
            </tr>
            </thead>
          <tbody>
            @forelse ($kategori as $key=>$item)
            <tr>
                
                <td>
                    {{ $key+1 }}
                </td>
                <td>
                    {{ $item->nama}}
                </td>
                <td class="project-actions ">
                    <form action="/kategori/{{ $item->id }}" method="post">
                        @csrf
                        @method('delete')
                        {{-- <a class="btn btn-primary btn-sm" href="/kategori/{{ $item->id }}">
                            <i class="fas fa-folder">
                            </i>
                            View
                        </a> --}}
                        <a class="btn btn-info btn-sm" href="/kategori/{{ $item->id }}/edit">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Edit
                        </a>

                        <button class="btn btn-danger btn-sm delete-confirm" data-name="{{ $item->nama }}" type="submit"><i class="fas fa-trash">
                        </i> 
                        Delete</button>
                    </form>
                </td>
            </tr>
            @empty
                <tr><td colspan="3"><center>Data Kategori Masih Kosong</center></td></tr>
            @endforelse
          </tbody>
      </table>
    </div>


  </div>
@endsection
@push("scripts")
 
@include('sweetalert::alert')
{{-- <script src="/admin/plugins/jquery/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="/admin/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="/admin/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
    <script type="text/javascript">
    
    $('.delete-confirm').click(function(event) {
      var form =  $(this).closest("form");
      var name = $(this).data("name");
      event.preventDefault();
      swal({
          title: `Apakah anda akan menghapus kategori ${name}?`,
          text: "Anda tidak akan bisa mengembalikan kategori ini!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((result) => {
        if (result) {
          form.submit();
        }
      });
  });
    </script>
@endpush