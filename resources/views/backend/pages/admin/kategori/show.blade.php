@extends("backend.layouts.user")

@section('judul')
    Update Data |
@endsection

@section("content")
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Edit Kategori</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="/kategori/{{ $kategori->id }}" method="post">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Kategori</label>
              <input type="text" class="form-control" value="{{ $kategori->nama }}" id="nama" name="nama">
            </div>
            @error('nama')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <input type="button" id="confirmedit" class="btn btn-danger" value="Edit"/>
          <button type="submit" id="btnupdate" class="btn btn-primary">Update</button>
          <a href="/kategori" class="btn btn-secondary">Kembali</a>
        </div>
      </form>
    </div>
  </div>
@endsection
@push("scripts")
<script type="text/javascript">
    $(document).ready(function() {
        $( ".form-control" ).prop( "disabled", true );
        $('#btnupdate').hide();
    });

    $("#confirmedit").click(function(){
        $('#btnupdate').show();
        $('#confirmedit').hide();
        $( ".form-control" ).prop( "disabled", false );
    });
    $(document).ready(function() {
        $( ".resize" ).prop( "disabled", true );
        $('#btnupdate').hide();
    });

    $("#confirmedit").click(function(){
        $('#btnupdate').show();
        $('#confirmedit').hide();
        $( ".resize" ).prop( "disabled", false );
    });
  </script>
@endpush