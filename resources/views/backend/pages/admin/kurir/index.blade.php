@extends("backend.layouts.user")

@section('judul')
    Dashboard |
@endsection

@section("content")
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Kurir</h3>
    </div>
    <div class="card-body p-0">
        <a href="/kurir/create" class="btn btn-primary m-3">Tambah Kurir</a>
      <table class="table table-striped projects">
          <thead>
              <tr>
                    <th style="width: 5%">
                        #
                    </th>
                    <th >
                        Nama
                    </th>
                    <th >
                        Harga
                    </th>
                    <th style="width: 25%">
                        Action
                    </th>
              </tr>
          </thead>
          <tbody>
            @forelse ($kurir as $key=>$item)
            <tr>
                
                <td>
                    {{ $key+1 }}
                </td>
                <td>
                    {{ $item->nama}}
                </td>
                <td>
                    {{ $item->harga}}
                </td>
                <td class="project-actions ">
                    <form action="/kurir/{{ $item->id }}" method="post">
                        @csrf
                        @method('delete')
                        {{-- <a class="btn btn-primary btn-sm" href="/kategori/{{ $item->id }}">
                            <i class="fas fa-folder">
                            </i>
                            View
                        </a> --}}
                        <a class="btn btn-info btn-sm" href="/kurir/{{ $item->id }}/edit">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Edit
                        </a>

                        <button class="btn btn-danger btn-sm delete-confirm" data-name="{{ $item->nama }}" type="submit"><i class="fas fa-trash">
                        </i> 
                        Delete</button>
                    </form>
                </td>
            </tr>
            @empty
                <tr><td colspan="4"><center>Data Kurir Masih Kosong</center></td></tr>
            @endforelse
          </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection
@push("scripts")
@include('sweetalert::alert')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
    
    $('.delete-confirm').click(function(event) {
      var form =  $(this).closest("form");
      var name = $(this).data("name");
      event.preventDefault();
      swal({
          title: `Apakah anda akan menghapus kurir ${name}?`,
          text: "Anda tidak akan bisa mengembalikan kurir ini!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((result) => {
        if (result) {
          form.submit();
        }
      });
  });
    </script>
@endpush