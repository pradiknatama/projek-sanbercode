@extends("backend.layouts.user")

@section('judul')
    Add Data |
@endsection

@section("content")
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah payment</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="/payment" method="post">
        @csrf
        <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama">
            </div>
            @error('nama')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
              <label for="exampleInputEmail1">No Rekening</label>
              <input type="text" class="form-control" id="rekening" name="rekening">
            </div>
            @error('rekening')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a href="/kategori" class="btn btn-secondary">Kembali</a>
        </div>
      </form>
    </div>
  </div>
@endsection