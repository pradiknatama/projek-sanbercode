@extends("backend.layouts.admin")
@push("style")
<!-- Select2 -->
<link rel="stylesheet" href="/admin/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush
@section("content")
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Edit Produk</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="/produk/{{ $produk->id }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama</label>
              <input type="text" class="form-control" id="nama" value="{{ $produk->nama }}" name="nama">
            </div>
            @error('nama')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label for="exampleInputEmail1">Harga</label>
                <input type="text" class="form-control" id="harga" value="{{ $produk->harga }}" name="harga">
            </div>
            @error('harga')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
               <label for="exampleInputEmail1">Stock</label>
               <input type="text" class="form-control" id="stock" value="{{ $produk->stock }}" name="stock">
            </div>
            @error('stock')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label for="exampleInputEmail1">Kategori</label>
                {{-- <input type="text" class="form-control" id="stock" value="{{ $produk->category->nama }}" name="stock"> --}}
                <select class="form-control js-example-basic-single" name="kategori">
                    @foreach ($produk->category->all() as $item)
                        @if ($produk->kategori_id==$item->id)
                            <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>        
                        @else
                            <option value="{{ $item->id }}" >{{ $item->nama }}</option>  
                        @endif
                    @endforeach
                </select>
            </div>
            @error('kategori')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label for="exampleInputEmail1">Deskripsi</label>
                <input type="text" class="form-control" id="deskripsi" value="{{ $produk->deskripsi }}" name="deskripsi">
             </div>
            @error('deskripsi')
             <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group pt-3">
                <label>Foto Produk</label><br>
                <img src="{{ asset('/images/produk/'.$produk->foto)}}" alt=""><br><br>
                <input type="file" class="form-control" name="foto">
            </div>
            @error('foto')
             <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <input type="button" id="confirmedit" class="btn btn-danger" value="Edit"/>
          <button type="submit" id="btnupdate" class="btn btn-primary">Update</button>
          <a href="/produk" class="btn btn-secondary">Kembali</a>
        </div>
      </form>
    </div>
  </div>
@endsection
@push("scripts")
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
   // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $( ".form-control" ).prop( "disabled", true );
        $('#btnupdate').hide();
    });

    $("#confirmedit").click(function(){
        $('#btnupdate').show();
        $('#confirmedit').hide();
        $( ".form-control" ).prop( "disabled", false );
    });
    $(document).ready(function() {
        $( ".resize" ).prop( "disabled", true );
        $('#btnupdate').hide();
    });

    $("#confirmedit").click(function(){
        $('#btnupdate').show();
        $('#confirmedit').hide();
        $( ".resize" ).prop( "disabled", false );
    });
  </script>
@endpush