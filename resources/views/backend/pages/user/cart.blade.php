@extends("backend.layouts.user")
@push("style")
  <style>
  .gambar{
    border-radius: 8px;
  max-height: 85px;
  }
  .card-details{
    border-radius: 11px;
    border-style: solid;
  border-color: #068EAC;
  }
  .Total{
    padding:10px 10px;

  }
  </style>
@endpush
@section("content")
<div class="page-content page-cart">
    <section
      class="store-breadcrumbs"
      data-aos="fade-down"
      data-aos-delay="100"
    >
      <div class="container">
        <div class="row">
          <div class="col-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">
                  Cart
                </li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </section>
    <section class="store-cart">
      <div class="container">
        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12 pl-lg-0  mb-2">
            <div class="card card-details">
            <table class="table table-responsive-xl  text-center cart-list" id="table-cart">
                <thead>
                  <tr>
                    {{-- <td><i class="fa fa-trash-o delete-all" style="font-size:24px" data-url="{{ url('delete-cart') }}"></i></td> --}}
                    <td>#</td>
                    <td>Gambar</td>
                    <td>Produk</td>
                    <td>Harga</td>
                    <td>Kuantitas</td>
                    <td>Subtotal</td>
                    <td>Action</td>
                    
                  </tr>
                </thead>
                <tbody>
                @php
                  $total=0;
                @endphp
                @forelse ($cart as $key=>$row)
                @php
                  $subtotal=0;
                  
                  $subtotal=$row->qty*$row->product->harga;
                  $total += $subtotal;
                @endphp
                <tr id="tr_{{$row->id}}">
                  <td scope="row">{{ $key+1 }}</td>
                  <td><img
                    src="{{ asset('/images/produk/'.$row->product->foto)}}"
                    alt=""
                    class="gambar" 
                  /></td>
                  <td class="produk">{{ $row->product->nama }}</td>
                  <td>Rp {{ number_format($row->product->harga) }}</td>
                  <td>{{ $row->qty }}</td>
                  <td><div style="width:90px">@currency($subtotal)</div></td>
                  <td class="project-actions ">
                    <form action="/cart/{{ $row->id }}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-danger btn-sm delete-confirm" data-name="{{ $row->nama }}" ><i class="fas fa-trash">
                        </i> 
                        Delete</button>
                    </form>
                </td>
                  
                </tr>
                
                @empty
                <tr><td colspan="7"><center>Data Produk Masih Kosong</center></td></tr>
            @endforelse
                </tbody>
              </table>
            </div>
          </div>
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr />
            </div>
            <form action="/checkout" method="POST" enctype="multipart/form-data">
              @csrf
            <div class="col-12">
              <h2 class="mb-4">Shipping Details</h2>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <input type="hidden" name="total" value="{{ $total }}">
            </div>
            <div class="form-group">
              <label for="country">Nama</label>
              <input
                type="text"
                class="form-control"
                id="country"
                name="nama"
                value="{{  Auth::user()->name }}"
              />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="mobile">Mobile</label>
              <input
                type="text"
                class="form-control"
                id="mobile"
                name="mobile"
                value="{{ Auth::user()->profil->phone }}"
              />
            </div>
          </div>
          <div class="row mb-2" data-aos="fade-up" data-aos-delay="200">
            <div class="col-md-12">
              <div class="form-group">
                <label for="addressOne">Alamat</label>
                <input
                  type="text"
                  class="form-control"
                  id="addressOne"
                  aria-describedby="emailHelp"
                  name="addressOne"
                  value="{{ Auth::user()->profil->alamat  }}"
                />
              </div>
            </div>
            
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="country">Kurir</label>
              <select name="kurir" id="kurir" class="form-control">
                @foreach ($kurir as $row)
                <option value="{{ $row->id }}">{{ $row->nama }}- @currency($row->harga)</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="country">Payment</label>
              <select name="payment" id="payment" class="form-control">
                @foreach ($payment as $row)
                <option value="{{ $row->id }}">{{ $row->nama }}- {{$row->rekening}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr />
            </div>

            <div class="col-12 col-md-12">
              <button
                type="submit"
                class="btn btn-success mt-4 px-4 btn-block"
              >
                Checkout Now
              </button>
            </div>
          </form>
          </div>      
        </div>
    </section>
    
</div>
@endsection
@push("scripts")

@include('sweetalert::alert')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
    
    $('.delete-confirm').click(function(event) {
      var form =  $(this).closest("form");
      var name = $(this).data("name");
      event.preventDefault();
      swal({
          title: `Apakah anda akan menghapus produk ${name} dari keranjang?`,
          text: "Anda tidak akan bisa mengembalikan produk ini!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((result) => {
        if (result) {
          form.submit();
        }
      });
  });
    </script>
@endpush