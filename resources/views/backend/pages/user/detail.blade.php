@extends("backend.layouts.user")
@push("style")
  <style>
    footer{
    position: relative;
    bottom: 0;
    left: 0;
    right: 0;
    height:70px;

  }
  </style>
@endpush
@section("content")
<div class="page-content page-details">
    <section
      class="store-breadcrumbs"
      data-aos="fade-down"
      data-aos-delay="100"
    >
      <div class="container">
        <div class="row">
          <div class="col-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">
                  Product Details
                </li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </section>
    <section class="store-gallery" id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-5" data-aos="zoom-in">
            <img src="{{ asset('/images/produk/'.$produk->foto)}}" alt="">
          </div>
          <div class="col-lg-7" data-aos="zoom-in">
            <h2>{{ $produk->nama }}</h2>

            <div class="price">@currency($produk->harga)</div>
            <hr>
            <form action="{{url('/cart/'.$produk->id)}}" method="POST" enctype="multipart/form-data">
              <div class="form-group row mb-3">
                <label for="qty" class="col-4 col-form-label">{{ __('Jumlah') }}</label>
  
                <div class="col-3">
                  <input id="qty" name="qty" type="number" class="form-control" min="1" required>
                    @error('qty')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                  @if ($produk->stock<=20)
                  <div class="col-4"> <h3>Tersisa {{ $produk->stock }} unit</h3></div>
                  @endif
              </div>

              @auth
                    @csrf
                    <button 
                      class="col-4 btn btn-success nav-link px-4 text-white btn-block mb-3"
                      type="submit" >Add to Cart</button>
            </form>
              @else
                <a 
                  class="col-4 btn btn-success nav-link px-4 text-white btn-block mb-3"
                  href="{{ route('login') }}" 
                  >Sign in to Add</a>
              @endauth
        </div>
      </div>
    </section>
    <div class="store-details-container" data-aos="fade-up">

      <section class="store-description">
        <div class="container">
          <hr>
          <div class="row">
            <div class="col-12 col-lg-12">
              <h2>Deskripsi Produk</h2>
              <p>
                {{ $produk->deskripsi }}
              </p>
            </div>
          </div>
        </div>
      </section>
      <section class="store-review">
        <div class="container">
          <div class="row">
            <div class="col-12 col-lg-12 mt-3 mb-3">
              <hr>
              <h1>Customer Review (3)</h1>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-8">
              <ul class="list-unstyled">
                <li class="media">
                  <img
                    src="/images/icon-testimonial-1.png"
                    class="mr-3 rounded-circle"
                    alt=""
                  />
                  <div class="media-body">
                    <h5 class="mt-2 mb-1">Testing 1</h5>
                    I thought it was not good for living room. I really happy
                    to decided buy this product last week now feels like
                    homey.
                    I thought it was not good for living room. I really happy
                    to decided buy this product last week now feels like
                    homey.
                    I thought it was not good for living room. I really happy
                    to decided buy this product last week now feels like
                    homey.
                    I thought it was not good for living room. I really happy
                    to decided buy this product last week now feels like
                    homey.
                  </div>
                </li>
                <li class="media my-4">
                  <img
                    src="/images/icon-testimonial-2.png"
                    class="mr-3 rounded-circle"
                    alt=""
                  />
                  <div class="media-body">
                    <h5 class="mt-2 mb-1">Testing 2</h5>
                    Color is great with the minimalist concept. Even I thought
                    it was made by Cactus industry. I do really satisfied with
                    this.
                  </div>
                </li>
                <li class="media">
                  <img
                    src="/images/icon-testimonial-3.png"
                    class="mr-3 rounded-circle"
                    alt=""
                  />
                  <div class="media-body">
                    <h5 class="mt-2 mb-1">testing 3</h5>
                    When I saw at first, it was really awesome to have with.
                    Just let me know if there is another upcoming product like
                    this.
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </div>
</div>
@endsection
@push("scripts")
<script src="/rating/jquery.barrating.js"></script>
<script src="/rating/rating-script.js"></script>
@endpush