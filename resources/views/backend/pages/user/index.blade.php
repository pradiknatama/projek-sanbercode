@extends("backend.layouts.user")

@section('judul')
    Dashboard |
@endsection

@section("header")
<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="5000">
            <div class="gambarBg" style="background-image: url(https://avatars.mds.yandex.net/i?id=d5aa3c76e99e81f26c2e65a98265f335-4593379-images-thumbs&n=13)"></div>
            <div class="carousel-caption d-none d-md-block">
                <h5 class="fw-bold">Selamat datang di 3A Store</h5>
            </div>
        </div>
        <div class="carousel-item" data-bs-interval="5000">
            <div class="gambarBg" style="background-image: url(https://avatars.mds.yandex.net/i?id=9931e9d755f48a90b710f6380da5e3c8-3780431-images-thumbs&n=13)"></div>
            <div class="carousel-caption d-none d-md-block">
                <h5 class="fw-bold">Barang Kualitas Penjabat</h5>
            </div>
        </div>
        <div class="carousel-item" data-bs-interval="5000">
            <div class="gambarBg" style="background-image: url(https://avatars.mds.yandex.net/i?id=a5bd168eb134e079aa071003ecfbdc10-5144219-images-thumbs&n=13)"></div> 
            <div class="carousel-caption d-none d-md-block">
                <h5 class="fw-bold">Harga Merakyat</h5>
            </div>
        </div>

    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
@endsection

@section("content")
<div style="justify-content: space-between; padding-bottom:10px">
    <div class="row">
        <div class="col">
            <form action="" style="padding-right:330px;">
                <div class="form-group">
                    <select name="kategori" id="" class="form-select">
                        <option value="">Kategori</option>
                        <option value="">Kategori</option>
    
                        {{-- @foreach ($kategori as $item)
                            <option value="{{ $item->$id }}">{{ $item->$nama }}</option>
                        @endforeach --}}
                    </select>
                </div>
            </form>
        </div>

        <div class="col">
            <form class="d-flex" style="padding-left:330px;">
                <input class="form-control me-2" type="search" aria-label="Search">
                <button class="btn btn-outline-secondary" type="submit">Search</button>
            </form>
        </div>
    </div>
</div>


<div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">    
    @forelse ($produk as $row)
        <div class="col mb-5">
            <div class="card h-100">
                <!-- Product image-->
                <img class="card-img-top" src="{{ asset('/images/produk/'.$row->foto)}}" alt="..." />
                <!-- Product details-->
                <div class="card-body p-4">
                    <div class="text-center">
                        <!-- Product name-->
                        <h5 class="fw-bolder">{{ $row->nama }}</h5>
                        <!-- Product price-->
                        @currency($row->harga)
                    </div>
                </div>
                <!-- Product actions-->
                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="/detail/{{ $row->id }}".>Detail</a></div>
                </div>
            </div>
        </div>
    @empty
        
    @endforelse

</div>
@endsection