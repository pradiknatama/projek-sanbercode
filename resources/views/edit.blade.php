@extends('layouts.master')

@section('isi')
    <form action="" method="POST" style="padding-bottom: 150px">
        @csrf
        @method('get')
            <div class="form-group">
                <label >Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
            </div>
            {{-- @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror --}}

            <div class="form-group pt-3">
                <label>Harga</label>
                <input type="number" class="form-control" name="harga" placeholder="Masukkan Harga">
            </div>
            {{-- @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror --}}

            <div class="form-group pt-3">
                <label>Stock</label>
                <input type="number" class="form-control" name="stock" placeholder="Masukkan Stock Barang">
            </div>

            {{-- <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
            </div> --}}
            {{-- @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror --}}

            <div class="form-group pt-3">
                <label >Status</label>
                <input type="text" class="form-control" name="status" placeholder="Masukkan Nama">
            </div>

            <div class="form-group pt-3">
                <label>Gambar</label>
                <input type="file" class="form-control" name="gambarBarang">
            </div>

            <div class="form-group" style="padding-top: 20px">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
    </form>
@endsection
