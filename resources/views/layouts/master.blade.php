<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>
            @yield('judul') 3A Store
        </title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <link rel = "shortcut icon" type = "image/png" href = "{{ asset('images\logoKecil.png') }}">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
        <link href="{{ asset('template1/css/styles.css') }}" rel="stylesheet" />
    </head>
    <body>
        @include('partials.nav')
        <!-- Section-->
        <section class="">
            <div class="">
                @yield('header')
            </div>
            <div class="container px-4 px-lg-5 mt-5">
                @yield('isi')
            </div>
        </section>
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2021</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ asset('template1/js/scripts.js') }}"></script>
    </body>
</html>
