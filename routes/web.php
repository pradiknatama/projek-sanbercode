<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('awal', function () {
    return view('index');
});
Route::get('edit', function () {
    return view('edit');
});
Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');


// backend
Route::get('/', 'ProdukController@user_show');
Route::get('/detail/{id}', 'ProdukController@user_detail');
//admin

Route::middleware(['role:1'])->group(function () {
    //kategori
    Route::get('/kategori', 'KategoriController@index')->name('kategori');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori/create', 'KategoriController@create');
    Route::get('/kategori/{id}/edit', 'KategoriController@edit');
    Route::put('/kategori/{id}', 'KategoriController@update');
    Route::delete('/kategori/{id}', 'KategoriController@destroy');

    //produk 
    Route::get('/produk', 'ProdukController@index')->name('produk');
    Route::post('/produk', 'ProdukController@store');
    Route::get('/produk/create', 'ProdukController@create');
    Route::delete('/produk/{id}', 'ProdukController@destroy');
    Route::get('/produk/{id}/edit', 'ProdukController@edit');
    Route::put('/produk/{id}', 'ProdukController@update');

    //payment
    Route::get('/payment', 'PaymentController@index')->name('payment');
    Route::post('/payment', 'PaymentController@store');
    Route::get('/payment/create', 'PaymentController@create');
    Route::get('/payment/{id}/edit', 'PaymentController@edit');
    Route::put('/payment/{id}', 'PaymentController@update');
    Route::delete('/payment/{id}', 'PaymentController@destroy');

    // kurir
    Route::get('/kurir', 'KurirController@index')->name('kurir');
    Route::post('/kurir', 'KurirController@store');
    Route::get('/kurir/create', 'KurirController@create');
    Route::get('/kurir/{id}/edit', 'KurirController@edit');
    Route::put('/kurir/{id}', 'KurirController@update');
    Route::delete('/kurir/{id}', 'KurirController@destroy');

});
Route::middleware(['role:2'])->group(function () {
    Route::get('/cart', 'CartController@index')->name('cart');
    Route::post('/cart/{id}', 'CartController@store');
    Route::delete('/cart/{id}', 'CartController@destroy');
    
    Route::get('/cart', 'CartController@index')->name('cart');
    Route::post('/cart/{id}', 'CartController@store');
    Route::get('/checkout', 'CheckoutController@index');
    Route::post('/checkout', 'CheckoutController@proses');
    Route::get('/laporan-pdf/{id}','CheckoutController@generatePDF');

});

